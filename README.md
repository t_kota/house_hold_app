# README #

This README would normally document whatever steps are necessary to get your application up and running.

### 環境構築 ###

1, expoのアカウントを作成する

https://expo.io

2, expoのアプリを自分のスマホにインストールし、アプリ上でログインする

3, Node.jsをインストールする

4, yarnをインストールする

5,  以下コマンドを入力して、expoをインストールする

`yarn global add expo-cli`

6,以下コマンドを実行すると、QRコードが表示されるのでスマホのカメラアプリで読み取ると、セットアップしたアプリにいい感じの画面が表示されたら完了！！

`expo start`

※うまく行かない場合は修正するので教えてください
